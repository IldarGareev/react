<!--
    Названия подключаемых файлов должны быть build/bundle.ext, так как они получат эти имена после компиляции.
    Компиляция из дериктории dev_tools, по командам :
        "npm run dev" | "webpack" версия для девелопа
        "npm run production" версия для продакшена
-->
<!doctype html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="build/bundle.d09fbb64aa58d5806e10.css">
        <title>Webpack</title>
    </head>
    <body>
        <div id="app"></div>
        <script src="build/bundle.d09fbb64aa58d5806e10.js"></script>
    </body>
</html>