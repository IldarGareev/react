<?php
    if (isset($_POST['firstNumber']) && isset($_POST['secondNumber'])) {
        $firstNumber = (int)trim($_POST['firstNumber']);
        $secondNumber = (int)trim($_POST['secondNumber']);
        $sum = $firstNumber + $secondNumber;
        echo json_encode(['sum' => $sum]);
    }