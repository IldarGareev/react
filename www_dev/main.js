import React from 'react';
import ReactDOM from 'react-dom';
import Game from './react_components/Game.jsx';

ReactDOM.render(
    <Game />,
    document.getElementById('app')
);