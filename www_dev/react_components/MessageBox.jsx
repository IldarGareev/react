import React, {Component} from 'react';
import '../css/MessageBox.css';

class MessageBox extends Component {

    constructor(props){
        super(props)
    }

    render() {
        if (!this.props.msg) {
            return null;
        }
        return (
            <div className="msg_box">
                {this.props.msg}
            </div>
        );
    }
}

export default MessageBox;