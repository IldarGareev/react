import React, { Component } from 'react';
import $ from "jquery";
import ReactDOM from 'react-dom';
import '../css/Form.css';
import MessageBox from './MessageBox.jsx';

class Form extends Component{

    constructor(props) {
        super(props);
        this.state = {
            msg: false,
            sum: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        ReactDOM.findDOMNode(this.refs.sum).focus();
    }

    handleChange(e) {
        this.setState({sum: e.target.value});
    }

    handleSubmit() {

        let data = {
            firstNumber: this.props.firstNumber,
            secondNumber: this.props.secondNumber
        };

        $.ajax({
            type: 'post',
            url: '/request.php',
            dataType: 'json',
            data: data,
            success: function(data) {
                let text = (data.sum == this.state.sum)
                    ? <div className="alert alert-success"><strong>Поздравляем!</strong> Вы ответили правильно</div>
                    : <div className="alert alert-danger"><strong>Извините!</strong> Сумма не верна, повторите попытку снова</div>;
                this.setState({msg: text});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
                console.log('fail');
            }.bind(this)
        });
    }

    render() {
        return (
            <div className="form">
                <div className="form-group">
                    <input type="text"
                           className="form-control"
                           value={this.state.sum}
                           onChange={this.handleChange}
                           ref="sum"/>
                </div>
                <div className="form-group">
                    <button onClick={this.handleSubmit}
                            className="btn btn-primary">
                        Проверить
                    </button>
                </div>
                <MessageBox msg={this.state.msg} />
            </div>
        );
    }
}

export default Form;