import React, {Component} from 'react';
import '../css/Game.css';
import Form from './Form.jsx';

class Game extends Component {

    constructor(props){
        super(props);
        this.state = {
            firstNumber: this.getRandomInt(1, 100),
            secondNumber: this.getRandomInt(1, 100),
        };
    }

    getRandomInt(min, max){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    render() {
        return (
            <div className="sum">
                <p>Введите сумму чисел: {this.state.firstNumber} + {this.state.secondNumber}</p>
                <Form firstNumber={this.state.firstNumber} secondNumber={this.state.secondNumber}/>
            </div>
        );
    }
}

export default Game;