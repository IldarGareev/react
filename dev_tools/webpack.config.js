var webpack = require('webpack');
var path = require('path');
var ChunkHashReplacePlugin = require('chunkhash-replace-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CleanWebpackPlugin = require('clean-webpack-plugin');
var inProduction = (process.env.NODE_ENV === 'production');

var buildDirectory = '../www/build';
var devDirectory = '../www_dev';

// the path(s) that should be cleaned
var pathsToClean = [
    path.resolve(__dirname, buildDirectory)
];

// the clean options to use
var cleanOptions = {
    root:     path.resolve(__dirname, '/../'),
    verbose:  true,
    dry:      false
};

module.exports = {
    entry: {
        bundle: [
            devDirectory + '/main.js',
            devDirectory + '/css/main.css',
            devDirectory + '/fonts/Roboto-Medium.ttf'
        ]
    },
    output: {
        path: path.resolve(__dirname, buildDirectory),
        filename: '[name].[chunkhash].js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader'],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.s[ac]ss$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader'],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.(svg|eot|ttf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: 'fonts/[name].[ext]'
                }
            },
            {
                test: /\.(png|jpe?g|gif)$/,
                loader: 'file-loader',
                options: {
                    name: 'images/[name].[ext]'
                }
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions),
        new ExtractTextPlugin('[name].[chunkhash].css'),
        new webpack.LoaderOptionsPlugin({
            minimize: inProduction
        }),
        new ChunkHashReplacePlugin({
            src: devDirectory + '/main.php',
            dest: buildDirectory + '/../index.php'
        })
    ]
};

if (inProduction) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin()
    );
}